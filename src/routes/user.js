const User = require('../models/user');

// Generate random token
const randomstring = require("randomstring");

// Enconde password
'use strict';



module.exports = function (app){  //aqui recibo objecto
  app.get('/usuario', (req,res) => {
      User.getUsers((err, data) => {
          res.status(200).json(data);
        }
      );
  });


  app.post('/usuario/registrar', (req,res) => {

      var random = randomstring.generate({
        length: 11,
        charset: 'hex'
      });

      var buff = new Buffer(req.body.Password);
      var base64data = buff.toString('base64');

      var userData = {
        Id: random,
        Nombre: req.body.Nombre,
        Apellido: req.body.Apellido,
        DocumentoIdentidad: req.body.DocumentoIdentidad,
        TipoDocumento: req.body.TipoDocumento,
        Email: req.body.Email,
        Password: base64data,
        Nacimiento: req.body.Nacimiento,
        Sexo: req.body.Sexo,
        NombreBanco: req.body.NombreBanco,
        NumeroCuenta: req.body.NumeroCuenta,
        TipoCuenta: req.body.TipoCuenta,
        EmailContactoBanco: req.body.EmailContactoBanco,
        Comision: 0,
        Estado : 0
      }

      User.insertUsuario(userData, (err, data) => {
          if(data){
            res.json({
              success: 200,
              status: 'OK',
              data: data
            });
          }else{
            res.json({
              success: false,
              msg: 'NO-OK'
            });
          }
        }
      );
  });



  app.post('/transaccion/registrar', (req,res) => {

    var random = randomstring.generate({
      length: 6,
      charset: 'hex'
    });

      var userData = {
        IdTrx: random,
        Monto: req.body.Monto,
        TipoMoneda: req.body.TipoMoneda,
        Detalle: req.body.Detalle,
        Comercio: req.body.Comercio,
        IdReferidor: req.body.IdReferidor,
        FechaTransaccion: req.body.FechaTransaccion,
        Pagada : "N"
      }

      User.insertTransaccion(userData, (err, data) => {
          if(data){
            res.json({
              success: 200,
              status: 'OK',
              IdTrx: userData.IdTrx
            });
          }else{
            res.json({
              success: false,
              msg: 'NO-OK'
            });
          }
        }
      );
  });


  app.put('/transaccion/pagar', (req,res) => {

      var userData = {
        IdTrx: req.body.IdTrx,
      }

      User.updateTransaccion(userData, (err, data) => {
          if(data){
            res.json({
              success: 200,
              status: 'OK',
              IdTrx: userData.IdTrx
            });
          }else{
            res.json({
              success: false,
              msg: 'NO-OK'
            });
          }
        }
      );
  });


}
