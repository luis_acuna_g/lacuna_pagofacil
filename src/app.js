const express = require('express');
const app = express();

const morgan = require('morgan');
const bodyparser  = require('body-parser');

// Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(bodyparser.json());

//Routers
require('./routes/user')(app);


app.listen(app.get('port'), () => {
  console.log('server port 3000');
});
