const mysql = require('mysql');


connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pagofacil'
  });

  let usermodel = {};

  usermodel.getUsers = (callback) => {
    if(connection){
      connection.query(
        'SELECT * FROM usuarios',
        (err, rows) => {
          if(err){
            throw err;
          }else{
            callback(null, rows);
          }
        }
      );
    }
  };

  usermodel.insertUsuario = (userData, callback) => {
    if(connection){
      connection.query(
        'INSERT INTO usuarios set ?',userData,
        (err, result) => {
          if(err){
            throw err;
          }else{
            callback(null, {'id': userData.Id});
          }
        }
      );
    }
  }


  usermodel.insertTransaccion = (userData, callback) => {
    if(connection){
      connection.query(
        'INSERT INTO transacciones set ?',userData,
        (err, result) => {
          if(err){
            throw err;
          }else{
            callback(null, {'id': userData.Id});
          }
        }
      );
    }
  }


  usermodel.updateTransaccion = (userData, callback) => {
    if(connection){
      connection.query(
        'UPDATE transacciones SET Pagada = "Y" where IdTrx = ?',[userData.IdTrx],
        (err, result) => {
          if(err){
            throw err;
          }else{
            callback(null, {'id': userData.Id});
          }
        }
      );
    }
  }





module.exports = usermodel;
