CREATE DATABASE pagofacil;

USE pagofacil;

CREATE TABLE Usuarios (
  Id VARCHAR(12) NOT NULL PRIMARY KEY,
  Nombre VARCHAR(50),
  Apellido VARCHAR(50),
  DocumentoIdentidad CHAR(12),
  TipoDocumento CHAR(12),
  Email VARCHAR(30),
  Password VARCHAR(20),
  Nacimiento DATE,
  Sexo CHAR(2),
  NombreBanco VARCHAR(50),
  NumeroCuenta VARCHAR(30),
  TipoCuenta VARCHAR(30),
  EmailContactoBanco VARCHAR(30),
  Comision INT(2),
  Estado VARCHAR(2)
);

CREATE TABLE Transacciones (
  IdTrx VARCHAR(12) NOT NULL PRIMARY KEY,
  Monto INT(10),
  TipoMoneda CHAR(5),
  Detalle VARCHAR(150),
  Comercio VARCHAR(100),
  IdReferidor VARCHAR(12),
  FechaTransaccion DATE COMMENT 'Fecha y hora de la transaccion',
  Pagada CHAR(2)
);
