### Installation
Requires  Apache y MYSQL (Xampp - Wampp)
Requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm i express body-parser morgan mysql
$ npm install randomstring
```
Load Database MYSQL (./db/pagofacil.sql)

Clone Project Bitbucket...

```sh
$ git clone https://luis_acuna_g@bitbucket.org/luis_acuna_g/lacuna_pagofacil.git
```

To execute the project you must execute the following command inside the directory

```sh
$ node src/app.js
```

### Method API
These are the methods declared in the API

| Method | URL | Description
| ------ | ------ |  ------
| GET | http://localhost:3000/usuario | List users system
| POST | http://localhost:3000/usuario/registrar | Register user
| POST | http://localhost:3000/transaccion/registrar | Register transaction
| PUT | http://localhost:3000/transaccion/pagar | Modify transaction

### Example

##### User
Method GET
Result:
```sh
[
  {
    "Id": "62cecb0f36b",
    "Nombre": "Mauricio",
    "Apellido": "Acuna",
    "DocumentoIdentidad": "17394613-9",
    "TipoDocumento": "RUT",
    "Email": "macuna@libretapp.cl",
    "Password": "MTIzNDU2",
    "Nacimiento": "1991-12-11T23:00:00.000Z",
    "Sexo": "M",
    "NombreBanco": "Estado",
    "NumeroCuenta": "123456",
    "TipoCuenta": "RUT",
    "EmailContactoBanco": "contacto@estado.cl",
    "Comision": 0,
    "Estado": 0
    },
  {
    "Id": "f77e65a91ce",
    "Nombre": "Mauricio",
    "Apellido": "Acuna",
    "DocumentoIdentidad": "1-9",
    "TipoDocumento": "RUT",
    "Email": "macuna@libretapp.cl",
    "Password": "MTIzNDU2",
    "Nacimiento": "1991-12-11T23:00:00.000Z",
    "Sexo": "M",
    "NombreBanco": "Estado",
    "NumeroCuenta": "123456",
    "TipoCuenta": "RUT",
    "EmailContactoBanco": "contacto@estado.cl",
    "Comision": 0,
    "Estado": 0
    }
],
```

##### Register User
Format JSON / Method POST / Request
```sh
{
	"Nombre" : "Mauricio",
  	"Apellido" : "Acuna",
    "DocumentoIdentidad": "2-7",
  	"TipoDocumento": "RUT",
  	"Email": "macuna@libretapp.cl",
  	"Password": "123456",
  	"Nacimiento": "1991-12-12",
  	"Sexo": "M",
  	"NombreBanco" : "Estado",
  	"NumeroCuenta" : "123456",
  	"TipoCuenta" : "RUT",
  	"EmailContactoBanco" : "contacto@estado.cl"
}
```
Result:
```sh
{
    "success": 200,
    "status": "OK",
    "data": {
        "id": "62cecb0f36b"
    }
}
```

##### Register Transaction
Format JSON / Method POST / Request

```sh
{
  "Monto": "10000",
  "TipoMoneda": "CLP",
  "Detalle": "Compra de poleras en un ecommerce",
  "Comercio": "Super ecommerce",
  "IdReferidor": "232h32DY33m242dsd4451",
  "FechaTransaccion": "2019-12-12"
}
```
Result:
```sh
{
    "success": 200,
    "status": "OK",
    "IdTrx": "99714f"
}
```

##### Modify Transaction
Format JSON / Method PUT / Request

```sh
{
  "IdTrx": "99714f"
}
```
